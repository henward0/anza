from django.shortcuts import render_to_response
from django.template import RequestContext

from .models import Campground
from helpers import createDateList

MONTHS = {  1: 'Jan',
            2: 'Feb',
            3: 'Mar',
            4: 'Apr',
            5: 'May',
            6: 'Jun',
            7: 'Jul',
            8: 'Aug',
            9: 'Sep',
            10: 'Oct',
            11: 'Nov',
            12: 'Dec'
         }

def home(request):
    """Homepage for Campensation"""
    available_list = Campground.objects.all().order_by('weekendDate')
    
    return render_to_response('campdata_home.html', {'available_list': available_list}, context_instance=RequestContext(request))


def results(request, currentDate=None):

    dateList = createDateList()
    if not currentDate:
        currentDate = dateList[0].isoformat()

    """ Campground filter rules """
    qs = Campground.objects.all()
    # Remove sites without photo
    qs = qs.exclude(faciltyPhoto__icontains="nophoto")
    qs = qs.filter(isTopSite=True)

    availableList = qs.filter(weekendDate=currentDate)


    return render_to_response('campgrounds.html', {'availableList': availableList,
                                                   'dateList': dateList,
                                                   'currentDate': currentDate,
                                                   'day': int(currentDate.split('-')[2]),
                                                   'month': MONTHS[int(currentDate.split('-')[1])],
                                                   'year': int(currentDate.split('-')[0])
                                                }, context_instance=RequestContext(request))