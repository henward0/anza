from django.conf.urls.defaults import *

urlpatterns = patterns('',
    url(r'^(?P<currentDate>.*)$', 'campdata.views.results'),
)