from django.core.management.base import BaseCommand, CommandError
from campdata.models import *
from django.db import connections
import urllib2
import urllib
from ptools.xmlloader import *
from helpers import createDateList, getTopSiteNames


import logging
# Get an instance of a logger
logger = logging.getLogger(__name__)
class NullHandler(logging.Handler): #exists in python 3.1
    def emit(self, record):
        pass
nullhandler = logger.addHandler(NullHandler())





ACTIVE_URL = 'http://api.amp.active.com'

CAMPING_SEARCH = '/camping/campgrounds?'

API_KEY = '98q62x5ymhchvhdge2f9bz42'


"""
	We have removed pets from the search.  If we want to search for campsites that allow then pets=3010 is the code
"""


"""
	CODES: siteType
	
	RV Sites	2001
	Cabins or Lodgings	10001
	Tent	2003
	Trailer	2002
	Group Site	9002
	Day Use	9001
	Horse Site	3001
	Boat Site	2004


	CODES: amenity
	
	Biking	4001
	Boating	4002
	Equipment Rental	4003
	Fishing	4004
	Golf	4005
	Hiking	4006
	Horseback Riding	4007
	Hunting	4008
	Recreational Activities	4009
	Scenic Trails	4010
	Sports	4011
	Beach/Water Activities	4012
	Winter Activities	4013
	
"""

DEFAULT_SITE_TYPE = '2003'
DEFAULT_AMENITY_TYPE = '4009'


class Command(BaseCommand):
    args = None
    help = 'Loads campground data'

    def handle(self, *args, **options):
        logger.info("STARTING NEW CAMPGROUND DATA LOAD")

        conn = connections['default']

        logger.info("Dropping Campground Database")
        conn.database.campdata_campground.drop()

        logger.info("Starting loadin by date")
        for fridayDate in createDateList():
            logger.info("Friday = %s" % fridayDate)
            dataList = campgroundSearch(arvdate = fridayDate.strftime("%m/%d/%Y"), lengthOfStay=2)

            for data in dataList:
                campgroundList = [Campground(weekendDate=fridayDate, **data) for data in dataList]

            availableList = [cg for cg in campgroundList if cg.availabilityStatus == 'Y']

            for cg in availableList:
                cg.save()

        logger.info('Pulling Top Sites')
        topSitesList = getTopSiteNames()

        logger.info('Saving top site matches')
        for cg in Campground.objects.filter(facilityName__in=topSitesList):
            cg.isTopSite = True
            cg.save()


def campgroundSearch(arvdate, pstate='CA', lengthOfStay=2, siteType=DEFAULT_SITE_TYPE, api_key=API_KEY):

    args = locals().copy()
    params = urllib.urlencode(args)

    f = urllib2.urlopen(ACTIVE_URL + CAMPING_SEARCH + params)

    logger.info(ACTIVE_URL + CAMPING_SEARCH + params)

    campgroundData = StreamXMLLoader(f, 0).expectXML()['resultset']['result']

    li = [campground['__attrs__'] for campground in campgroundData]

    return li


def campground_loader():
    conn = connections['default']
    conn.database.campdata_campground.drop()


    for fridayDate in createDateList():
        logger.info("Friday = %s" % fridayDate)
        dataList = campgroundSearch(arvdate = fridayDate.strftime("%m/%d/%Y"), lengthOfStay=2)

        for data in dataList:
            campgroundList = [Campground(weekendDate=fridayDate, **data) for data in dataList]

        availableList = [cg for cg in campgroundList if cg.availabilityStatus == 'Y']

        for cg in availableList:
            cg.save()
    
