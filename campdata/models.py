from django.db import models

# Create your models here.

class Campground(models.Model):

    agencyIcon = models.CharField(max_length=30)
    agencyName = models.CharField(max_length=30)
    availabilityStatus = models.CharField(max_length=1)
    contractID = models.CharField(max_length=30)
    contractType = models.CharField(max_length=30)
    facilityID = models.CharField(max_length=30)
    facilityName = models.CharField(max_length=30)
    faciltyPhoto = models.CharField(max_length=60)
    latitude = models.CharField(max_length=30)
    longitude = models.CharField(max_length=30)
    regionName = models.CharField(max_length=30)
    reservationChannel = models.CharField(max_length=30)
    shortName = models.CharField(max_length=30)
    sitesWithAmps = models.CharField(max_length=1)
    sitesWithPetsAllowed = models.CharField(max_length=1)
    sitesWithSewerHookup = models.CharField(max_length=1)
    sitesWithWaterHookup = models.CharField(max_length=1)
    sitesWithWaterfront = models.CharField(max_length=1)
    state = models.CharField(max_length=2)
    weekendDate = models.DateField()
    isTopSite = models.BooleanField(default=False)
