from datetime import *
from dateutil.relativedelta import *
import urllib2
import urllib
from ptools.xmlloader import *
import os

import settings

TOP_CAMPGROUNDS = os.path.join(settings.PROJECT_PATH, 'docs/top_campgrounds.txt')


def createDateList():
	
	TODAY = date.today()
	
	FRIDAY = TODAY + relativedelta(weekday=FR)
	
	li = [FRIDAY]
	
	for i in range(1,10):
		li.append(li[-1] + relativedelta(days=+1, weekday=FR))
	
	return li


def getTopSiteNames():
    API_KEY = '98q62x5ymhchvhdge2f9bz42'
    ACTIVE_URL = 'http://api.amp.active.com'
    CAMPING_SEARCH = '/camping/campgrounds?'
    params = urllib.urlencode({'pstate':'CA', 'api_key':API_KEY})

    page = urllib2.urlopen(ACTIVE_URL + CAMPING_SEARCH + params)

    campgroundData = StreamXMLLoader(page, 0).expectXML()['resultset']['result']

    campgroundList = [campground['__attrs__'] for campground in campgroundData]

    checkNames = lambda x,y: len(set(x.lower().split()).intersection(set(y.lower().split()))) > 1

    top_sites = open(TOP_CAMPGROUNDS).read().splitlines()

    result = []

    for cg in campgroundList:
        for site in top_sites:
            if checkNames(cg['facilityName'], site):
                result.append(cg['facilityName'])

    return list(set(result))