from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.contrib import admin
admin.autodiscover()

import campdata

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^', include('campdata.urls')),
    url(r'^admin/', include(admin.site.urls)),


    # STATIC FILES
    (r'^static/(?P<path>.*)/$', 'django.views.static.serve',),

)
